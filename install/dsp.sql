/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50547
Source Host           : localhost:3306
Source Database       : test1

Target Server Type    : MYSQL
Target Server Version : 50547
File Encoding         : 65001

Date: 2019-02-28 00:18:41
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `ms_dsp_admin`
-- ----------------------------
DROP TABLE IF EXISTS `ms_dsp_admin`;
CREATE TABLE `ms_dsp_admin` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `nickname` varchar(100) DEFAULT NULL,
  `qq` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ms_dsp_admin
-- ----------------------------
INSERT INTO `ms_dsp_admin` VALUES ('1', 'admin', 'b8d9ab78f76573b3d753547c9ce28b12', '默笙', '3494490');

-- ----------------------------
-- Table structure for `ms_dsp_info`
-- ----------------------------
DROP TABLE IF EXISTS `ms_dsp_info`;
CREATE TABLE `ms_dsp_info` (
  `id` int(1) unsigned NOT NULL AUTO_INCREMENT,
  `logo` varchar(100) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `descc` varchar(255) DEFAULT NULL,
  `banquan` varchar(255) DEFAULT NULL,
  `banquan1` varchar(255) DEFAULT NULL,
  `isvip` int(2) DEFAULT '1',
  `islogin` int(2) DEFAULT '1',
  `istype` int(2) DEFAULT '0',
  `dspurl` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ms_dsp_info
-- ----------------------------
INSERT INTO `ms_dsp_info` VALUES ('1', 'MS\'S VIDEO', '默笙短视频解析-抖音皮皮虾微视去水印解析', null, null, 'creat by MoSheng ', 'made with <i class=\"material-icons\">favorite</i> by             MoSheng for a better web.', '0', '0', '0', 'https://api.tecms.net/dsp?token=&key=&url=');

-- ----------------------------
-- Table structure for `ms_dsp_interface`
-- ----------------------------
DROP TABLE IF EXISTS `ms_dsp_interface`;
CREATE TABLE `ms_dsp_interface` (
  `id` int(100) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `status` int(2) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ms_dsp_interface
-- ----------------------------
INSERT INTO `ms_dsp_interface` VALUES ('1', '抖音', '0');
INSERT INTO `ms_dsp_interface` VALUES ('2', '皮皮虾', '0');
INSERT INTO `ms_dsp_interface` VALUES ('3', '微视', '0');
INSERT INTO `ms_dsp_interface` VALUES ('4', '快手', '0');
INSERT INTO `ms_dsp_interface` VALUES ('6', '火山', '0');

-- ----------------------------
-- Table structure for `ms_dsp_user`
-- ----------------------------
DROP TABLE IF EXISTS `ms_dsp_user`;
CREATE TABLE `ms_dsp_user` (
  `id` int(200) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(200) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `qq` varchar(50) DEFAULT NULL,
  `status` int(2) DEFAULT '0',
  `isvip` int(2) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ms_dsp_user
-- ----------------------------

-- ----------------------------
-- Table structure for `ms_dsp_vip`
-- ----------------------------
DROP TABLE IF EXISTS `ms_dsp_vip`;
CREATE TABLE `ms_dsp_vip` (
  `id` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(255) DEFAULT NULL,
  `begintime` varchar(255) DEFAULT NULL,
  `endtime` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ms_dsp_vip
-- ----------------------------

-- ----------------------------
-- Table structure for `ms_dsp_vipkami`
-- ----------------------------
DROP TABLE IF EXISTS `ms_dsp_vipkami`;
CREATE TABLE `ms_dsp_vipkami` (
  `id` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `viptext` varchar(255) DEFAULT NULL,
  `type` int(3) DEFAULT NULL,
  `status` int(2) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ms_dsp_vipkami
-- ----------------------------
