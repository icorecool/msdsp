<?php
namespace Home\Controller;

use Think\Controller;

class UserController extends Controller
{
	//判断是否登录
    public function _initialize()
    {
        if ($_SESSION['user']['id']=="") {
           $this->redirect('login/index');
        }
    }
    public function index()
    {
    	$id=(int)$_SESSION['user']['id'];
    	$userinfo=M('user')->where("id=$id")->find();
		$admininfo=M('admin')->where("id=1")->find();
    	$adminqq=$admininfo['qq'];
    	$adminname=$admininfo['nickname'];
    	$vip=M('vip')->where("uid=$id")->find();
    	if (!empty($vip)) {
    		$data['isvip']=0;
    		$re=M('user')->where("id=$id")->save($data);
    	}
    	$vipendtime=$vip['endtime'];//vip结束时间

    	if ($vipendtime<time()) {
    		$data['isvip']=1;
    		$re=M('user')->where("id=$id")->save($data);
    	}
    	$this->info();
    	$this->assign('userinfo',$userinfo);
    	$this->assign('adminqq',$adminqq);
    	$this->assign('adminname',$adminname);
    	$this->assign('vipendtime',$vipendtime);
        $this->display();
    }
    //退出登录
    public function LoginOut()
    {
        session_destroy();
        $this->success('退出成功',U('Login/index'));
    }
    public function ktvip()
    {
    	$id=(int)$_SESSION['user']['id'];
    	$k=M('vipkami');
    	$kami=remove_xss(trim($_POST['km']));
    	$re=$k->where("viptext='$kami'")->find();
    	
    	if (empty($re)) {
    		$this->error("卡密不存在，请检查");
    	}elseif(!empty($re)){
    		
    		$sta=(int)$re['status'];
    		if ($sta==1) {
    			$this->error("该卡密已被使用");
    		}else{
	    		$type=(int)$re['type'];
	    		//判断用户是否vip
	    		$isvip=M('user')->where("id=$id")->find();
	    		$isvip=(int)$isvip['isvip'];
				
	    		if ($isvip==0) {
					$vipuser=M('vip')->where("uid=$id")->find();
	    			$time=(int)$vipuser['endtime'];
	    			if ($time>time()) { //判断会员是否到期
	    				if ($type==1) {
	    					$data['endtime']=strtotime("+1month",$time);//+30天
							$ktvip=M('vip')->where("uid=$id")->save($data);
	    					if (!empty($ktvip)) {
	    						$vipkm['status']=1;
	    						$isuse=$k->where("viptext='$kami'")->save($vipkm);
	    						$this->success("续费成功",U('user/index'));
	    					}else{
	    						$this->error("续费失败",U('user/index'));
	    					}
	    				}
		    			if ($type==2) {
							$data['endtime']=strtotime("+1year",$time);//+30天
	    					$ktvip=M('vip')->where("uid=$id")->save($data);
	    					if (!empty($ktvip)) {
	    						$vipkm['status']=1;
	    						$isuse=$k->where("viptext='$kami'")->save($vipkm);
	    						$this->success("续费成功",U('user/index'));
	    					}else{
	    						$this->error("续费失败",U('user/index'));
	    					}
		    			}
	    			}else{
	    				//到期
	    				

	    			}
	    			
	    			
	    		}
	    		if ($isvip==1) {
	    			$user['isvip']=1;
	    			$vipuser=M('user')->where("uid=$id")->save($user);
	    			$data['begintime']=time();
	    			$data['uid']=$id;
	    			$time=time();
	    			if ($type==1) {
	    				$data['endtime']=strtotime("+1month",$time);//+30天
						$ktvip=M('vip')->where("uid=$id")->add($data);
	    				if (!empty($ktvip)) {
	    					$vipkm['status']=1;
	    					$isuse=$k->where("viptext='$kami'")->save($vipkm);
	    					$this->success("开通成功",U('user/index'));
	    				}else{
	    					$this->error("开通失败",U('user/index'));
	    				}
	    			}
		    		if ($type==2) {
						$data['endtime']=strtotime("+1year",$time);//+30天
	    				$ktvip=M('vip')->where("uid=$id")->add($data);
	    				if (!empty($ktvip)) {
	    					$vipkm['status']=1;
	    					$isuse=$k->where("viptext='$kami'")->save($vipkm);
	    					$this->success("开通成功",U('user/index'));
	    				}else{
	    					$this->error("开通失败",U('user/index'));
	    				}
		    		}
	    		}
    		}
    	}else{

    		
    	}

    }
    public function upinfo()
    {
    	$id=(int)$_SESSION['user']['id'];
    	$data['password']=md5(remove_xss(trim($_POST['pass'])));
    	$re=M('user')->where("id=$id")->save($data);
    	if (!empty($re)) {
    		$this->success("修改成功，下次登录生效",U('user/index'));
    	}else{
    		$this->error("修改失败，不能与原密码相同",U('user/index'));
    	}

    }
    function info(){
      $info=M('info')->where("id=1")->find();
      $this->assign('info',$info);
    }
    
}
