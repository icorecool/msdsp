<?php
namespace Admin\Controller;
use Think\Controller;
class IndexController extends Controller {
	//判断是否登录
	public function _initialize(){
	    if ($_SESSION['admin']['id']=="") {
           $this->redirect('login/index');
        }
	}
    //退出登录
    public function LoginOut(){
        session_destroy();
        $this->success('退出成功',U('login/index'));
    }
    public function index(){
    	$usercount=M('user')->count();
        $this->assign('usercount',$usercount);
        $this->display();
    }
    public function insta(){
        $info=M('info')->where("id=1")->find();
        $istype=(int)$info['istype'];
        if ($istype==0) {
            $this->assign('istype',$istype);
        }else{
            $list=M('interface')->order("id DESC")->select();
            $this->assign('istype',$istype);
            $this->assign('list',$list);
        }
        $this->display();
    }
    public function inadd(){
        if (!IS_AJAX) {
            $this->display();
        }else{
            $n=remove_xss(I('post.name','','strip_tags'));
            $data['title']=remove_xss(I('post.name','','strip_tags'));
            $data['status']=(int)remove_xss(I('post.sta','','strip_tags'));
            $re=M('interface')->where("title='$n'")->select();
            if (!empty($re)) {
                $this->error("该接口名称已存在，请更换");
            }else{
                $add=M('interface')->add($data);
                if (!empty($add)) {
                    $this->success("新增成功",U('index/insta'));
                }else{
                    $this->error("新增失败");
                }
            }
        }
    }
    //禁用
    public function injy(){
        if (!IS_AJAX) {
            $this->error("非法请求");
        }else{
            $id=(int)remove_xss(I('post.id','','strip_tags'));
            $data['status']=1;
            $re=M('interface')->where("id=$id")->save($data);
            if (!empty($re)) {
                $this->success("状态修改成功",U('index/insta'));
            }else{
                $this->error("状态修改失败");
            }
        }
    }
    //启用
    public function inqy(){
        if (!IS_AJAX) {
            $this->error("非法请求");
        }else{
            $id=(int)remove_xss(I('post.id','','strip_tags'));
            $data['status']=0;
            $re=M('interface')->where("id=$id")->save($data);
            if (!empty($re)) {
                $this->success("状态修改成功",U('index/insta'));
            }else{
                $this->error("状态修改失败");
            }
        }
    }
    //删除
    public function indel(){
        if (!IS_AJAX) {
            $this->error("非法请求");
        }else{
            $id=(int)remove_xss(I('post.id','','strip_tags'));
            $re=M('interface')->where("id=$id")->delete();
            if (!empty($re)) {
                $this->success("删除成功！",U('index/insta'));
            }else{
                $this->error("删除失败");
            }
        }
    }
    //开启远程获取
    public function kqyc(){
        if (!IS_AJAX) {
            $this->error("非法请求");
        }else{
            $data['istype']=0;
            $re=M('info')->where("id=1")->save($data);
            if (!empty($re)) {
                $this->success("设置成功！",U('index/insta'));
            }else{
                $this->error("设置失败");
            }
        }
    }
    //关闭远程获取
    public function gbyc(){
        if (!IS_AJAX) {
            $this->error("非法请求");
        }else{
            $data['istype']=1;
            $re=M('info')->where("id=1")->save($data);
            if (!empty($re)) {
                $this->success("设置成功！",U('index/insta'));
            }else{
                $this->error("设置失败");
            }
        }
    }
    
}